defmodule EvernowWeb.FileController do
  alias Evernow.FileUploader

  def upload_file(_, %{file: file}, %Absinthe.Resolution{context: %{claims: %{"sub" => user_id}}}) do
    {:ok, uploading_file(file, user_id)}
  end

#  to avoid code crash, in case of file not received as file in params
  def upload_file(_, _, _) do
    {:error, ["invalid params"]}
  end

  defp uploading_file(file, user_id) do
    case FileUploader.store({file, %{storage: user_id}}) do
      {:ok, name} ->
        # making storage directory on date time
        path = FileUploader.storage_dir(1, {file, %{storage: user_id}})
        # two objects original and thumb is returned
        %{
          original: "#{path}original/#{name}",
          thumb: "#{path}thumb/#{name}"
        }
      {:error, :invalid_file} -> %{error: ["Invalid file format, upload blocked!"]}
      a ->
        %{error: ["Something went wrong, file upload failed"]}
    end
  end
end
