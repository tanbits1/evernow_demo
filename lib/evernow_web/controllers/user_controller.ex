defmodule EvernowWeb.UserController do
  import Sage
  alias Evernow.Accounts.User
  alias Evernow.Accounts

  def signup(params) do

    new()
    |> run(:email_taken, &is_email_taken/2, &abort/3)
    |> run(:user, &create_user/2, &cancel_user_creation/3)
    |> transaction(Evernow.Repo, params)
  end

  defp is_email_taken(_, %{email: email}) do
    IO.inspect(email)
    case Accounts.get_user_by_email(email) do
      nil ->
        {:ok, ["valid"]}
      %User{} -> {:error, ["Email has been already taken"]}
    end
  rescue
    _ -> {:error, ["error in checking email is taken"]}
  end

  defp create_user(%{email_taken: ["valid"]}, user_params) do
    user_params |> Accounts.create_user()
  rescue
    all ->
      IO.inspect(all)
      {:error, ["error in creating user"]}
  end

  defp cancel_user_creation(%{user: user}, _params, _attrs) do
    user |> Accounts.delete_user()
    :abort
  rescue
    _ -> {:error, ["error in deleting user"]}
  end

  defp cancel_user_creation(_effect_to_compensate, _params, _attrs) do
    :abort
  end
  defp abort(_, _, _) do
    :abort
  end
end
