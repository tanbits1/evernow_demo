defmodule EvernowWeb.Resolvers.UserResolver do
  alias Evernow.{Accounts, Posts}
  alias EvernowWeb.UserController

  def users(_, _, _) do
    {:ok, Accounts.list_users()}
  end

  def create_user(_, %{input: input}, _) do
    case UserController.signup(input) do
      {:ok, _, %{user: user}} -> {:ok, user}
      {:error, changeset} -> {:error, changeset}
      _ -> {:error, ["Something went wrong"]}
    end
  rescue
    _ -> {:error, ["something went wrong"]}
  end

  def login(_, %{input: input}, _) do
    with {:ok, user} <- Accounts.authenticate(input),
         {:ok, jwt_token, _} <- Evernow.Guardian.encode_and_sign(user)  do
      {:ok, %{user: user, token: jwt_token}}
    else
      {:error, changeset} -> {:error, changeset}
      _ -> {:error, ["Something went wrong"]}
    end
  rescue
    _ -> {:error, ["something went wrong"]}
  end

  def logout(_, %{input: %{token: token}}, %Absinthe.Resolution{context: %{claims: %{"sub" => user_id}}}) do
    case Evernow.Guardian.revoke(token) do
      {:ok, _} -> {:ok, "logout successfully"}
      _ -> {:error, ["unable to logout"]}
    end
  rescue
    _ -> {:error, ["something went wrong"]}
  end

  def build_video(_, _, %Absinthe.Resolution{context: %{claims: %{"sub" => user_id}}}) do
    # Remove previously timelapse before proceeding to creating a new one.
    Evernow.Utils.Builder.delete_timelapse(user_id)

    # Enqueue Building timelapse in background.
    case Evernow.Utils.Builder.get_user_frames(user_id) do
      [] -> {:error, :no_frames_uploaded}
      _files ->
        build_video_in_background(user_id)
        {:ok, :video_building_process_enqueued}
    end
  end

  def build_video_in_background(user_id) do
    Task.async(fn ->
      # Build the video timelapse.
      builder_response = Evernow.Utils.Builder.build(user_id)

      # Remove the frames/images for that user.
      File.rm_rf("files/#{user_id}")

      # Show the response which we saved earlier.
      case builder_response do
        {:error, :no_frame_found} -> {:error, %{message: "There is no frame yet uploaded by user."}}
        {:error, error} -> {:error, %{message: :video_could_not_be_created_right_now}}
        {:ok, timelapse_link} -> {:ok, timelapse_link}
      end
    end)
  end

  def build_video(_, _, _), do: {:error, :must_require_valid_jwt}

  # Get video of current user (if exists)
  def get_video(_, _, %Absinthe.Resolution{context: %{claims: %{"sub" => user_id}}}) do
    timelapse_link = Evernow.Utils.Builder.timelapse_file_path(user_id)
    if File.exists?(timelapse_link) do
      {:ok, timelapse_link}
    else
      {:error, :file_not_found}
    end
  end

  def get_video(_, _, _), do: {:error, :must_require_valid_jwt}
end