defmodule EvernowWeb.Plugs.ParseToken do
  @behaviour Plug
  import Plug.Conn

  def init(_) do
    nil
  end

  def call(conn, _) do
    authorization = List.last(get_req_header(conn, "authorization"))
    context = build_context(authorization)
    IO.inspect(context, label: "context")
    ip = conn.remote_ip |> :inet.ntoa() |> to_string()
    put_private(conn, :absinthe, %{context: Map.merge(context, %{ip: ip})})
  end

  def build_context(authorization) do
    case extract_token(authorization) do
      {:ok, token} ->
        case parse_and_verify_token(token) do
          {:ok, jwt_claims} ->
            %{claims: jwt_claims}
          {:error, _} -> %{}
        end

      {:error, _} ->
        %{}
    end
  end

  defp extract_token(authorization) do
    case authorization do
      nil ->
        {:error, "No authorization header provided"}

      authorization ->
        bearer_regex = ~r/\Abearer /i

        case Regex.match?(bearer_regex, authorization) do
          true -> {:ok, Regex.replace(bearer_regex, authorization, "")}
          false -> {:error, "Authorization header should start with 'Bearer'"}
        end
    end
  end

  defp parse_and_verify_token(token) do
    Evernow.Guardian.decode_and_verify(token)
  end
end
