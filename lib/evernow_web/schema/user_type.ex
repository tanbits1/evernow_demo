defmodule EvernowWeb.Schema.Types.UserType do
  use Absinthe.Schema.Notation

  object :user_type do
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :profile_image, :json
    field :mobile, :string
    field :birth_at, :string
    field :gender, :string
    field :address, :string
  end

  object :session_type do
    field :token, :string
    field :user, :user_type
  end

  input_object :user_input_type do
    field :email, :string
    field :password, non_null :string
    field :first_name, :string
    field :last_name, :string
    field :profile_image, :file
    field :mobile, :string
    field :birth_at, :string
    field :gender, :string
    field :address, :string
  end

 input_object :user_login_type do
    field :email, non_null(:string)
    field :password, non_null(:string)
  end

 input_object :user_logout_type do
    field :token, non_null(:string)
  end

  input_object :file do
    field :thumb, :string
    field :original, :string
  end
end
