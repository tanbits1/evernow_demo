defmodule Evernow.Repo do
  use Ecto.Repo,
    otp_app: :evernow,
    adapter: Ecto.Adapters.Postgres
end
