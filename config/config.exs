# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :evernow,
  ecto_repos: [Evernow.Repo]

# Configures the endpoint
config :evernow, EvernowWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "f2RH98fv2He3l8/xibiiohWOHpkQs9LekGDb0aDE0nazrSo3FGd4UT1vVHhZKdHR",
  render_errors: [view: EvernowWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Evernow.PubSub, adapter: Phoenix.PubSub.PG2]

config :evernow, Evernow.Guardian,
       issuer: "evernow_demo",
       secret_key: "RV/89k8ddk5RgC3iL6kLpCqlEaHgShsG2MDE9SJ59j+pCbra9H4Ve1ewy3ruhHuc"

config :guardian, Guardian.DB,
       repo: Evernow.Repo,
       schema_name: "guardian_tokens",
       sweep_interval: 10080  # 1 week 60*24*7
#       sweep_interval: 1

config :arc,
       storage: Arc.Storage.Local,
       storage_dir: "files"
#       bucket: {:system, "AWS_S3_BUCKET"} # if using Amazon S3

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
